FROM gradle:jdk11 as build-stage
WORKDIR /app
COPY ./ ./
RUN ./gradlew clean build

FROM openjdk:11 as production-stage
COPY --from=build-stage /app/build/libs/app.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]