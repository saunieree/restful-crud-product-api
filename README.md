# About
RESTful application using Java and Spring Boot that has an API supporting the basic CRUD operations for products and allows to:

- Create a new product
- Retrieve a list of all products
- Update a product
- Delete a product (soft deletion)

# Usage

The application is containerized and can be built, tested and run with docker using following command:

```
docker build -t product-api . && docker run -p 8080:8080 product-api
``` 

## Building

The application can be build with following gradle command:

```
./gradlew build
```

## Testing

A set of automated acceptance tests written in a behavior-driven development (BDD) style and can be run with following command:

```
./gradlew test
```

## Running

The application can be run locally with:

```
./gradlew bootRun
```

and accessed `http://localhost:8080/` where the HAL Browser helps to explore the API.

# Storage

A solution for persisting the API's data is provided with Postgres database.

Initial configuration of database is required and is described below.

If database is initialised the the application can be started with following command:

```
docker-compose up
```

## Local database initialization

Start the database container with a volume configuration to persist the database beyond container lifecycle and with the arbitrary postgres admin password:

```
docker run --name product-database -d --rm -p 5432:5432 -v ~/product_postgres_data:/var/lib/postgresql/data:z -e POSTGRES_PASSWORD=pg postgres
```


Connect to the container and run postgres client:

```
docker exec -it product-database bash
psql -U postgres
```

Execute the script in order to create a database and a user for the service.

```
CREATE DATABASE product_dev;
CREATE USER product_user WITH ENCRYPTED PASSWORD 'insecure';
GRANT ALL PRIVILEGES ON DATABASE product_dev TO product_user;
```


You can now stop the container and start the services with the docker-compose configuration.


