package com.mgladki.product;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(regex("/products.*"))

                .build().apiInfo(
                        new ApiInfo(
                                "Product API",
                                "API supporting the basic CRUD operations for products",
                                "1.0.0",
                                null,
                                new Contact("Maciej", "https://gitlab.com/saunieree/restful-crud-product-api", "maciej.gladki@gmail.com"),
                                null,
                                null,
                                Collections.emptyList()
                        )
                );
    }
}