package com.mgladki.product.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.persistence.*;
import java.time.ZonedDateTime;

@ApiModel(description = "Product Entity")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Product {

    /**
     * SKU (unique id) of the product
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="product_generator")
    @SequenceGenerator(name="product_generator", sequenceName = "PRODUCT_ID_SQ", allocationSize = 1)
    @ApiModelProperty(hidden = true)
    private Long id;

    /**
     * Name of the product
     */
    @ApiModelProperty(notes = "Name of the product", example = "Reverse")
    @Size(min = 2, max = 255, message
            = "Name of the product must be between 2 and 255 characters")
    private String name;

    /**
     * Price of the product in CHF
     */
    @ApiModelProperty(notes = "Price of the product in CHF, must be greater than 0")
    @NotNull
    @Min(value = 0, message = "Price of the product must be greater than 0")
    private Float priceInChf;

    /**
     * Date the product was created
     */
    @ApiModelProperty(notes = "Date the product was created. Set automatically.")
    private ZonedDateTime createdDate;

    /**
     * Soft deletion flag
     */
    @ApiModelProperty(hidden = true)
    boolean isDeleted;

}
