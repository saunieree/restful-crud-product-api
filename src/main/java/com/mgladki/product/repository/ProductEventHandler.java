package com.mgladki.product.repository;

import com.mgladki.product.entity.Product;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
@Component
@RepositoryEventHandler
public class ProductEventHandler {

    @HandleBeforeCreate
    public void handleProductSave(Product product) {
        product.setCreatedDate(ZonedDateTime.now());
    }
}
