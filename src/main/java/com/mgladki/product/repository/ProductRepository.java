package com.mgladki.product.repository;

import com.mgladki.product.entity.Product;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {

    @ApiOperation(value = "Delete product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK") })
    @Modifying
    @Query("update Product p set isDeleted = true where p = :p")
    void delete(Product p);

    @ApiOperation(value = "Get all products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK") })
    @Query("select p FROM Product p WHERE p.isDeleted = false")
    Iterable<Product> findAll();
}
