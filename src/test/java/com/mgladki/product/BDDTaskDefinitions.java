package com.mgladki.product;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@CucumberContextConfiguration
public class BDDTaskDefinitions {

    @Autowired
    private MockMvc mockMvc;

    private MockHttpServletResponse response;

    @When("^client calls /products")
    public void theClientIssuesGetProducts() throws Throwable {
        response = mockMvc.perform(get("/products"))
                .andReturn().getResponse();
    }

    @Then("^client receives status code of (\\d+)$")
    public void theClientReceivesStatus(int statusCode) {
        assertThat(response.getStatus(), is(statusCode));
    }

    @And("^client receives list of (\\d+) products$")
    public void theClientReceivesListOfProducts(int expectedNumberOfProducts) throws Throwable {
        int numberOfProducts = JsonPath.read(response.getContentAsString(),"$._embedded.products.length()");
        assertThat(numberOfProducts, is(expectedNumberOfProducts));
    }


    @When("^client posts to /products API")
    public void clientPostsProduct() throws Throwable {
        response = mockMvc.perform(
                post("/products")
                .content(new JSONObject()
                        .put("name", "renverse")
                        .put("priceInChf", 1.8)
                        .toString())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    @And("^client receives URI to the product$")
    public void clientReceivesProduct() throws Throwable {
        assertThat(JsonPath.read(response.getContentAsString(),"$._links.self.href"), endsWith("/products/1"));
    }

    @When("^client updates product with id (\\d+)$")
    public void clientUpdatesProduct(int id) throws Throwable {
        response = mockMvc.perform(
                patch("/products/" + id)
                        .content(new JSONObject()
                                .put("priceInChf", 1.9)
                                .toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    @When("^client deletes product with id (\\d+)$")
    public void clientDeletesProduct(int id) throws Throwable {
        response = mockMvc.perform(
                delete("/products/" + id))
                .andReturn().getResponse();
    }
}
