Feature: CRUD operation of product API

  Scenario: Create a new product
    When client posts to /products API
    Then client receives status code of 201
    And client receives URI to the product

  Scenario: Retrieve a list of all products
    When client calls /products
    Then client receives status code of 200
    And client receives list of 1 products

  Scenario: Update a product
    When client updates product with id 1
    Then client receives status code of 200
    And client receives URI to the product

  Scenario: Delete a product
    When client deletes product with id 1
    Then client receives status code of 204

  Scenario: Retrieve a list of all products
    When client calls /products
    Then client receives status code of 200
    And client receives list of 0 products